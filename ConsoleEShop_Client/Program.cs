﻿using System;
using ConsoleEShop_Client.State;
using ConsoleEShop_Data;

namespace ConsoleEShop_Client
{
    static class Program
    {
        static void Main(string[] args)
        {
            // Create a Context with start state as Guest Menu
            var context = new Context(new GuestMenu());

            // Start work
            context.Request1();
        }
    }
}
