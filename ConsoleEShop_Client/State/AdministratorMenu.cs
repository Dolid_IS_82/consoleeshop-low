﻿using ConsoleEShop_BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleEShop_Client.State
{
    public class AdministratorMenu : ResitredUserMenu
    {

        public AdministratorMenu(string login) : base(login)
        { }

        public override List<Tuple<string, MenuItems>> CreateMenu()
        {
            menuItems = new List<Tuple<string, MenuItems>>()
            {
                new Tuple<string, MenuItems>("List of goods", ListOfGoods),
                new Tuple<string, MenuItems>("Search goods by name", Search),
                new Tuple<string, MenuItems>("Create Order", CreateOrder),
                new Tuple<string, MenuItems>("Confirm or cancel the order ", ConfirmCancelOrder),
                new Tuple<string, MenuItems>("Edit profiles", ChangeUsersData),
                new Tuple<string, MenuItems>("Add new goods", AddNewGoods),
                new Tuple<string, MenuItems>("Changes goods info", ChangeGoodsInfo),
                new Tuple<string, MenuItems>("Changes orders status", ChangeOrdersStatus),
                new Tuple<string, MenuItems>("Get all orders", GetAllOrders),
                new Tuple<string, MenuItems>("Log of", LogOf)
            };


            return menuItems;
        }

        /// <summary>
        /// Displays information about all users (not admins) in the console and allows you to edit it 
        /// </summary>
        public void ChangeUsersData()
        {
            Console.Clear();
            var users = userService.GetAllUser();
            if(users == null)
            {
                Console.WriteLine("No users found ");
                return;
            }

            Console.WriteLine("\t\tUsers");
            Console.WriteLine($"{"Login",-10} {"Password",-10} {"Name",-10} Type");
            foreach (var item in users)
            {
                if(item.Type == UserTypeDTO.RegisteredUser)
                    Console.WriteLine($"{item.Login, -10} {item.Password, -10} {item.Name, -10} {item.Type}");
            }

            Console.WriteLine("Enter the login of the user whose information you want to change:");
            string userLogin = Console.ReadLine();

            if (!userService.LoginExist(userLogin))
            {
                Console.WriteLine("Incorrect login!!!");
                return;
            }

            Console.WriteLine("Enter new password: ");
            string password = Console.ReadLine();
            Console.WriteLine("Enter new password again: ");
            string passwordSecond = Console.ReadLine();

            if (password != passwordSecond)
                Console.WriteLine("Passwords do not match!");
            else
            {
                Console.WriteLine("Enter your name:");
                string name = Console.ReadLine();
                userService.ChangeUserData(new UserDTO()
                {
                    Login = userLogin,
                    Password = password,
                    Name = name,
                    Type = UserTypeDTO.RegisteredUser
                });
            }

        }

        /// <summary>
        /// Allows the admin to add a new product to the product list 
        /// </summary>
        public void AddNewGoods()
        {
            Console.Clear();
            Console.WriteLine("\t Add new item");
            Console.Write("Enter name: ");
            var name = Console.ReadLine();
            Console.Write("Enter category: ");
            var category = Console.ReadLine();
            Console.Write("Enter description: ");
            var description = Console.ReadLine();

            double inputDouble;
            while (true)
            {
                Console.Write("Enter price: ");
                string inputPrice = Console.ReadLine();
                if (Double.TryParse(inputPrice, out inputDouble))
                    break;

                Console.WriteLine("Incorrect price. Try again!!!!");
            }
            
            goodsService.AddGoods(new GoodsDTO() 
            { 
                Name = name, 
                Category = category, 
                Description = description, 
                Price = inputDouble 
            });
            Console.WriteLine("Successfully added !!!");
        }

        /// <summary>
        /// Displays a list of all products in the console and allows the administrator
        /// to change the information on a particular product 
        /// </summary>
        public void ChangeGoodsInfo()
        {
            Console.Clear();
            var goods = goodsService.GetAllGoods().ToList();
            if (goods == null)
            {
                Console.WriteLine("No goods found ");
                return;
            }

            Console.WriteLine("\t List of goods");
            Console.WriteLine($"№  {"Name",-10} {"Price",-10} {"Category",-15}   Description");
            for (int i = 0; i < goods.Count; i++)
            {
                Console.WriteLine($"{i+1}  {goods[i].Name,-10} {goods[i].Price,-10} " +
                    $"{goods[i].Category,-10} {goods[i].Description}");
            }

            Console.WriteLine("Enter the item number you want to change");
            string inputNo = Console.ReadLine();

            int inputInt;
            if (!Int32.TryParse(inputNo, out inputInt) || inputInt <= 0 || inputInt > goods.Count)
            {
                Console.WriteLine("Incorrect goods!!!");
                return;
            }

            inputInt--;
            Console.WriteLine($"Name: {goods[inputInt].Name}  Category: {goods[inputInt].Category}");
            Console.Write("Enter new description: ");
            var description = Console.ReadLine();

            double inputDouble;
            while (true)
            {
                Console.Write("Enter new price: ");
                string inputPrice = Console.ReadLine();
                if (Double.TryParse(inputPrice, out inputDouble))
                    break;

                Console.WriteLine("Incorrect price. Try again!!!!");
            }

            var result = goodsService.ChangeGoodsData(new GoodsDTO() 
            {
                Name = goods[inputInt].Name,
                Category = goods[inputInt].Category,
                Description = description,
                Price = inputDouble
            });

            if (result)
                Console.WriteLine("Successfully changed!");
            else
                Console.WriteLine("Something went wrong");
        }

        /// <summary>
        /// Displays a list of all orders in the console and allows the administrator to change their status 
        /// </summary>
        public void ChangeOrdersStatus()
        {
            GetAllOrders();

            Console.Write("Select order by id: ");
            string idString = Console.ReadLine();

            int idInt;
            if (!Int32.TryParse(idString, out idInt) || idInt < 0)
            {
                Console.WriteLine("Incorrect id!!!!");
                return;
            }

            Console.WriteLine("\nSelect the order status you want to set");
           
            for (int i = 1; i <= 7; i++)
                Console.WriteLine($"{i}. {(OrderStatusDTO)i}");
            Console.Write("Status: ");
            var statusString = Console.ReadLine();

            int statusInt;
            if (!Int32.TryParse(statusString, out statusInt) || statusInt <= 0 || statusInt > 7)
                Console.WriteLine("Incorrect status!!!!");
            else
                orderService.ChangeOrderStatus(idInt, (OrderStatusDTO)statusInt);
        }

        /// <summary>
        /// Displays a list of all orders 
        /// </summary>
        public void GetAllOrders()
        {
            Console.Clear();
            Console.WriteLine("\t\t All orders");
            var orders = orderService.GetAllOrders();

            if (!orders.Any())
                Console.WriteLine("Orders not found");

            foreach (var item in orders)
            {
                Console.WriteLine("Order status: " + item.OrderStatusDTO);
                Console.WriteLine("Order id: " + item.Id);
                Console.WriteLine("Ordered by: " + item.UserLogin);
                Console.WriteLine("\tOrder Data");
                Console.WriteLine($"№  {"Name",-10} {"Price",-10} {"Category",-15}   Description");
                for (int i = 0; i < item.Goods.Count; i++)
                {
                    Console.WriteLine($"{i + 1}. {item.Goods[i].Name, -10} {item.Goods[i].Price, -10} " +
                        $"{item.Goods[i].Category, -15} {item.Goods[i].Description}");
                }
                Console.WriteLine();
                Console.WriteLine();
            }
        }

    }
}
