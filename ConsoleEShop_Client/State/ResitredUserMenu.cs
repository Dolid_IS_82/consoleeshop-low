﻿using ConsoleEShop_BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleEShop_Client.State
{
    public class ResitredUserMenu : GuestMenu
    {
        protected List<GoodsDTO> selectedGoods;
        protected readonly string userLogin;

        public ResitredUserMenu(string login) => userLogin = login;

        public override List<Tuple<string, MenuItems>> CreateMenu()
        {
            menuItems = new List<Tuple<string, MenuItems>>()
            {
                new Tuple<string, MenuItems>("List of goods", ListOfGoods),
                new Tuple<string, MenuItems>("Search goods by name", Search),
                new Tuple<string, MenuItems>("Create Order", CreateOrder),
                new Tuple<string, MenuItems>("Confirm or cancel the order ", ConfirmCancelOrder),
                new Tuple<string, MenuItems>("Edit profile", ChangeUserData),
                new Tuple<string, MenuItems>("Order history", OrderHistory),
                new Tuple<string, MenuItems>("Set order status 'Get'", SetGetOrderStatus),
                new Tuple<string, MenuItems>("Log of", LogOf)
            };

            return menuItems;
        }

        /// <summary>
        /// Allows the user to create an order based on the list of products displayed in the console 
        /// </summary>
        public void CreateOrder()
        {
            bool run = true;
            selectedGoods = new List<GoodsDTO>();

            while (run)
            {
                Console.Clear();
                Console.WriteLine("\t Create Order");
                ListOfGoods();
                List<GoodsDTO> goodsList = goodsService.GetAllGoods()?.ToList();

                Console.Write("Choose № of product or press 'x' to exit: ");

                string input = Console.ReadLine();
                if (input == "x")
                    return;

                int inputInt;
                if (!Int32.TryParse(input, out inputInt))
                {
                    Console.WriteLine("Incorrect №!!!!");
                    continue;
                }

                if(inputInt > 0 && inputInt <= goodsList.Count)
                    selectedGoods.Add(goodsList[inputInt - 1]);
                else
                    Console.WriteLine("Incorrect №!!!!");
            }

        }

        /// <summary>
        /// Displays the order in the console and allows the user to confirm or cancel it 
        /// </summary>
        public void ConfirmCancelOrder()
        {
            Console.Clear();
            Console.WriteLine("\t Your order");
            Console.WriteLine($"№  {"Name",-10} {"Price",-10} {"Category",-15}   Description");


            if (selectedGoods == null)
            {
                Console.WriteLine("You have not selected any products, please return to the menu and create an order.");
                return;
            }


            for (int i = 0; i < selectedGoods.Count; i++)
            {
                Console.WriteLine($"{i + 1}. {selectedGoods[i].Name,-10} {selectedGoods[i].Price,-1} " +
                    $"{selectedGoods[i].Category,-15} {selectedGoods[i].Description}");
            }

            Console.Write("Confirm order y/n: ");
            string result = Console.ReadLine();
            if (result == "y")
            {
                orderService.CreateOrder(selectedGoods, userLogin);
                selectedGoods = null;
            }
            else if (result == "n")
                selectedGoods = new List<GoodsDTO>();
            else
                Console.WriteLine("Incorrect command!!");
        }

        /// <summary>
        /// Allows the user to change the password to their account 
        /// </summary>
        public void ChangeUserData()
        {
            Console.Clear();
            Console.WriteLine("\tPersonal Data");

            var user = userService.GetUserByLogin(userLogin);
            if(user == null)
            {
                Console.WriteLine("Something went wrong ");
                return;
            }
            Console.WriteLine($"Login: {userLogin}");
            Console.WriteLine($"Password: {user.Password}");
            Console.WriteLine($"Name: {user.Name}");
            Console.WriteLine($"Type: {user.Type}\n");

            Console.WriteLine("Enter 'p' if you want to change password or other key to exit");
            if (Console.ReadLine() != "p")
                return;

            Console.WriteLine("Enter password: ");
            string password = Console.ReadLine();
            Console.WriteLine("Enter password again: ");
            string passwordSecond = Console.ReadLine();

            if(password == passwordSecond)
            {
                user.Password = password;
                userService.ChangeUserData(user);
                Console.WriteLine("Password changed");
            }
            else
                Console.WriteLine("Passwords do not match");
        }

        /// <summary>
        /// Displays a list of all user orders to the console 
        /// </summary>
        public void OrderHistory()
        {
            Console.Clear();
            Console.WriteLine("\tOrders history");
            var orders = orderService.GetOrdersByUserLogin(userLogin).ToList();

            if (!orders.Any())
                Console.WriteLine("Orders not found");

            foreach (var item in orders)
            {
                Console.WriteLine("Order status: " + item.OrderStatusDTO);
                Console.WriteLine("Order id: " + item.Id);
                Console.WriteLine("\tOrder Data");
                Console.WriteLine($"№  {"Name",-10} {"Price",-10} {"Category",-15}   Description");
                for (int i = 0; i < item.Goods.Count; i++)
                {
                    Console.WriteLine($"{i + 1}. {item.Goods[i].Name,-10} {item.Goods[i].Price,-10} " +
                        $"{item.Goods[i].Category,-15} {item.Goods[i].Description}");
                }
                Console.WriteLine();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Allows the user to confirm receipt of the order
        /// </summary>
        public void SetGetOrderStatus()
        {
            OrderHistory();

            Console.Write("Select order by id: ");
            string idString = Console.ReadLine();

            int idInt;
            if (!Int32.TryParse(idString, out idInt))
            {
                Console.WriteLine("Incorrect id!!!!");
                return;
            }

            if (idInt > 0)
                orderService.ChangeOrderStatus(idInt, OrderStatusDTO.Get);
            else
                Console.WriteLine("Incorrect id!!!!");
        }


        public void LogOf()
        {
            this._context.TransitionTo(new GuestMenu());
        }

    }
}
