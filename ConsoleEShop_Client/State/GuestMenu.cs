﻿using ConsoleEShop_BLL.DTO;
using ConsoleEShop_BLL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleEShop_Client.State
{
    public class GuestMenu : MenuState
    {

        /// <summary>
        /// We create a set of methods that are items on Guest Menu
        /// </summary>
        /// <returns>List of delegates with their description  </returns>
        public override List<Tuple<string, MenuItems>> CreateMenu()
        {
            menuItems = new List<Tuple<string, MenuItems>>()
            {
                new Tuple<string, MenuItems>("List of goods", ListOfGoods),
                new Tuple<string, MenuItems>("Search goods by name", Search),
                new Tuple<string, MenuItems>("Log in", LogIn),
                new Tuple<string, MenuItems>("Regisrtation", Registration)
            };

            return menuItems;
        }

        /// <summary>
        /// Displays a list of all available products in the console  
        /// </summary>
        public void ListOfGoods()
        {
            List<GoodsDTO> goodsList = goodsService.GetAllGoods()?.ToList();

            if(goodsList == null)
            {
                Console.WriteLine("Goods not found");
                return;
            }

            Console.Clear();
            Console.WriteLine("\t List of goods");
            Console.WriteLine($"№  {"Name",-10} {"Price",-10} {"Category",-15}   {"Description",-30}");
            for (int i = 0; i < goodsList.Count; i++)
            {
                Console.WriteLine($"{i + 1}  {goodsList[i].Name,-10} {goodsList[i].Price,-10} " +
                    $"{goodsList[i].Category, -15} {goodsList[i].Description,-30}");
            }

        }

        /// <summary>
        /// Searches for products by the keyword entered by the user
        /// in the console and display the result
        /// </summary>
        public void Search()
        {
            Console.Clear();
            Console.Write("Enter name of product: ");
            string name = Console.ReadLine();
            List<GoodsDTO> goodsList = goodsService.FindGoodsByName(name).ToList();

            if (goodsList == null)
            {
                Console.WriteLine("Goods not found");
                return;
            }

            Console.WriteLine("\t List of goods");
            Console.WriteLine($"№  {"Name",-10} {"Price",-10} {"Category",-15}   {"Description",-30}");
            for (int i = 0; i < goodsList.Count; i++)
            {
                Console.WriteLine($"{i + 1}  {goodsList[i].Name,-10} {goodsList[i].Price,-10} " +
                    $"{goodsList[i].Category,-15} {goodsList[i].Description,-30}");
            }

        }

        /// <summary>
        /// Checks the user-specified login and password and in the presence
        /// of such a user changes the state of the system depending on the user type 
        /// </summary>
        public void LogIn()
        {
            Console.Clear();
            Console.WriteLine("Enter login: ");
            string login = Console.ReadLine();

            Console.WriteLine("Enter password: ");
            string password = Console.ReadLine();

            if (!userService.UserExist(login, password))
            {
                Console.WriteLine("Incorrect login or password");
                return;
            }


            var user = userService.GetUserByLogin(login).Type;
            if (user == UserTypeDTO.RegisteredUser)
            {
                this._context.TransitionTo(new ResitredUserMenu(login));
                Console.WriteLine("Succcess!!!");
            }
            else if (user == UserTypeDTO.Administrator)
            {
                this._context.TransitionTo(new AdministratorMenu(login));
                Console.WriteLine("Succcess!!!");
            }
            else
                Console.WriteLine("Something wrong. Try Again");
        }

        /// <summary>
        /// Allows the user to log in to the system by entering data in the appropriate fields on the console 
        /// </summary>
        public void Registration()
        {
            Console.Clear();

            Console.WriteLine("Enter login: ");
            string login = Console.ReadLine();

            Console.WriteLine("Enter password: ");
            string password = Console.ReadLine();
            Console.WriteLine("Enter password again: ");
            string passwordSecond = Console.ReadLine();

            if(userService.LoginExist(login))
                Console.WriteLine("This login already exist");
            else if(password != passwordSecond)
                Console.WriteLine("Passwords do not match");
            else
            {
                Console.WriteLine("Enter your name");
                string name = Console.ReadLine();
                userService.AddUser(new UserDTO()
                {
                    Login = login,
                    Password = password,
                    Name = name,
                    Type = UserTypeDTO.RegisteredUser
                });
            }


        }
    }
}
