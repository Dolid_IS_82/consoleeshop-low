﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Client.State
{
    /// <summary>
    /// The Context defines the interface of interest to clients. It also
    /// maintains a reference to an instance of a State subclass, which
    /// represents the current state of the Context.
    /// </summary>
    public class Context
    {
        /// <summary>
        /// A reference to the current state of the Context.
        /// </summary>
        private MenuState _state = null;

        public Context(MenuState state)
        {
            this.TransitionTo(state);
        }

        /// <summary>
        /// Changes state Menu to another at runtime
        /// </summary>
        /// <param name="state"></param>
        public void TransitionTo(MenuState state)
        {
            this._state = state;
            this._state.SetContext(this);
        }

        /// <summary>
        /// The Context delegates part of its behavior to the current State object.
        /// </summary>
        public void Request1()
        {
            bool run = true;

            while (run)
            {
                Console.Clear();
                var menu = this._state.CreateMenu();

                Console.WriteLine("\t\t Menu");

                for (int i = 1; i <= menu.Count; i++)
                {
                    Console.WriteLine(i + ". " + menu[i-1].Item1);
                }

                Console.WriteLine($"{menu.Count + 1}. Exit");
                Console.Write("Select a menu item:  ");

                int enterItem;
                if (!Int32.TryParse(Console.ReadLine(), out enterItem))
                {
                    Console.WriteLine("Incorrect item");
                    continue;
                }


                if (enterItem <= menu.Count && enterItem > 0)
                    menu[enterItem - 1].Item2();
                else if (enterItem == menu.Count + 1)
                    run = false;

                Console.Write("Press any key to continue: ");
                Console.ReadLine();

            }
        }
    }
}
