﻿using ConsoleEShop_BLL.Services;
using ConsoleEShop_Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Client.State
{
    /// <summary>
    ///The base State class declares methods that all Concrete State should
    /// implement and also provides a backreference to the Context object,
    /// associated with the State. This backreference can be used by States to
    /// transition the Context to another State.
    /// </summary>
    public abstract class MenuState
    {
        /// <summary>
        /// Context references for work with states  
        /// </summary>
        protected Context _context;

        /// <summary>
        ///  Delegate for menu items
        /// </summary>
        public delegate void MenuItems();

        /// <summary>
        /// Contains all methods for that type of menu
        /// </summary>
        protected List<Tuple<string, MenuItems>> menuItems;


        /// <summary>
        /// Service for work with Goods
        /// </summary>
        protected GoodsService goodsService;

        /// <summary>
        /// Service for work with User
        /// </summary>
        protected UserService userService;

        /// <summary>
        /// Service for work with Order
        /// </summary>
        protected OrderService orderService;

        /// <summary>
        /// Unit of Work for work with the database 
        /// </summary>
        protected static readonly ListUnitOfWork Uof = new ListUnitOfWork();

        protected MenuState()
        {
            goodsService = new GoodsService(Uof);
            orderService = new OrderService(Uof);
            userService = new UserService(Uof);
        }

        /// <summary>
        /// Set the context for the menu 
        /// </summary>
        /// <param name="context"></param>
        public void SetContext(Context context)
        {
            this._context = context;
        }

        /// <summary>
        /// We create a set of methods that are items on our menu 
        /// </summary>
        /// <returns>List of delegates with their description  </returns>
        public abstract List<Tuple<string, MenuItems>> CreateMenu();
    }
}
