﻿using ConsoleEShop;
using ConsoleEShop_BLL.DTO;
using ConsoleEShop_BLL.Interfaces;
using ConsoleEShop_Data;
using ConsoleEShop_Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleEShop_BLL.Services
{
    public class OrderService : IOrderService
    {
        IListUnitOfWork Database { get; set; }
        public OrderService(IListUnitOfWork iow)
        {
            Database = iow;
        }

        /// <summary>
        /// Get a list of orders made by this user 
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">Thrown if parameter was null</exception>
        public IEnumerable<OrderDTO> GetOrdersByUserLogin(string login)
        {
            if (login == null)
                throw new ArgumentNullException(nameof(login), "login equal null");

            return MyMapper.OrderListToOrderDTOEnumerable(Database.Orders.FindAll(x => x.User.Login == login));
        }

        public IEnumerable<OrderDTO> GetAllOrders()
        {
            return MyMapper.OrderListToOrderDTOEnumerable(Database.Orders);
        }

        /// <summary>
        /// Create a new order 
        /// </summary>
        /// <param name="goodsDTO">Set of goods in the ordered </param>
        /// <param name="login">The login of the user who placed this order </param>
        /// <exception cref="ArgumentNullException">Thrown if parameter was null</exception>
        /// <exception cref="ElementExistException">Thrown if item already exist</exception>
        public void CreateOrder(List<GoodsDTO> goodsDTO, string login)
        {
            if (goodsDTO == null || login == null)
                throw new ArgumentNullException(nameof(goodsDTO), "Argument equal null");

            var user = Database.Users.Find(x => x.Login == login);

            if (user == null)
                throw new ElementExistException("User with such login doesn't exist");

            var goods = MyMapper.GoodsDTOListToGoodsList(goodsDTO);

            Database.Orders.Add(new Order() { Goods = goods, OrderStatus = OrderStatus.New, User = user });
        }

        /// <summary>
        /// Change the status of order with thus id
        /// </summary>
        /// <param name="id">Id of the order</param>
        /// <param name="orderStatusDTO">New status</param>
        /// <returns>True if status was chnged, false otherwise</returns>
        /// <exception cref="ArgumentNullException">Thrown if parameter was null</exception>
        /// <exception cref="ElementExistException">Thrown if item already exist</exception>
        public bool ChangeOrderStatus(int id, OrderStatusDTO orderStatusDTO)
        {
            if (id < 0 || orderStatusDTO == 0)
                throw new ArgumentNullException("Incorrect argument");

            var index = Database.Orders.FindIndex(x => x.Id == id);
            if (index == -1)
                throw new ElementExistException("This irder doesn’t exist");

            if (Database.Orders[index].OrderStatus == (OrderStatus)orderStatusDTO)
                return false;

            Database.Orders[index].OrderStatus = (OrderStatus)orderStatusDTO;
            return true;
        }
    }
}
