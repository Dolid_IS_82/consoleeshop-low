﻿using AutoMapper;
using ConsoleEShop;
using ConsoleEShop_BLL.DTO;
using ConsoleEShop_BLL.Interfaces;
using ConsoleEShop_Data;
using ConsoleEShop_Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleEShop_BLL.Services
{
    public class UserService : IUserService
    {
        IListUnitOfWork Database { get; set; }
        public UserService(IListUnitOfWork iow)
        {
            Database = iow;
        }

        public bool LoginExist(string login)
        {
            return Database.Users.Any(x => x.Login == login);
        }

        /// <summary>
        /// Add a new registered user 
        /// </summary>
        /// <param name="userDTO"></param>
        /// <exception cref="ArgumentNullException">Thrown if parameter was null</exception>
        /// <exception cref="ElementExistException">Thrown if item already exist</exception>
        public void AddUser(UserDTO userDTO)
        {
            if (userDTO == null)
                throw new ArgumentNullException(nameof(userDTO), "Argument equal null");
            if (LoginExist(userDTO.Login))
                throw new ElementExistException("User already exist");

            Database.Users.Add(MyMapper.UserDTOToUser(userDTO));
        }


        public bool UserExist(string login, string password)
        {
            return Database.Users.Any(x => x.Login == login && x.Password == password);
        }

        /// <summary>
        /// Edit user information 
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">Thrown if parameter was null</exception>
        public bool ChangeUserData(UserDTO userDTO)
        {
            if (userDTO == null)
                throw new ArgumentNullException(nameof(userDTO), "Argument equal null");

            var index = Database.Users.FindIndex(x => x.Login == userDTO.Login);

            if (index == -1)
                return false;

            Database.Users[index] = MyMapper.UserDTOToUser(userDTO);
            return true;
        }

        public IEnumerable<UserDTO> GetAllUser()
        {
            return MyMapper.UserListToUserDTOEnumerable(Database.Users);
        }

        /// <summary>
        /// Get a user by this login 
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">Thrown if parameter was null</exception>
        public UserDTO GetUserByLogin(string login)
        {
            if (login == null)
                throw new ArgumentNullException(nameof(login), "Login is null");

            var user = Database.Users.Find(x => x.Login == login);

            return MyMapper.UserToUserDTO(user);
        }
    }
}
