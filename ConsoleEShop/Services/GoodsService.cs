﻿using AutoMapper;
using ConsoleEShop;
using ConsoleEShop_BLL.DTO;
using ConsoleEShop_BLL.Interfaces;
using ConsoleEShop_Data;
using ConsoleEShop_Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleEShop_BLL.Services
{
    public class GoodsService : IGoodsService
    {
        /// <summary>
        /// Property to access the database 
        /// </summary>
        IListUnitOfWork Database { get; set; }

        public GoodsService(IListUnitOfWork iow)
        {
            Database = iow;
        }

        public IEnumerable<GoodsDTO> GetAllGoods()
        {
            return MyMapper.GoodsListToGoodsDTOEnumerable(Database.Goods);
        }

        /// <summary>
        /// Receive item by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>One item with that name if exist</returns>
        /// <exception cref="ArgumentNullException">Thrown if parameter was null</exception>
        public GoodsDTO GetGoodsByName(string name)
        {
            if(name == null)
                throw new ArgumentNullException(nameof(name), "Argument equal null");

            return MyMapper.GoodsToGoodsDTO(Database.Goods.Find(x => x.Name == name));
        }

        /// <summary>
        /// Add a new product 
        /// </summary>
        /// <param name="goodsDTO"></param>
        /// <exception cref="ArgumentNullException">Thrown if parameter was null</exception>
        /// <exception cref="ElementExistException">Thrown if item already exist</exception>
        public void AddGoods(GoodsDTO goodsDTO)
        {
            if (goodsDTO == null)
                throw new ArgumentNullException(nameof(goodsDTO), "Argument equal null");

            if (GoodsExist(goodsDTO.Name))
                throw new ElementExistException("Goods already exist");

            Database.Goods.Add(MyMapper.GoodsDTOToGoods(goodsDTO));
        }

        /// <summary>
        /// Change the information about the available product 
        /// </summary>
        /// <param name="goodsDTO"></param>
        /// <returns>True if element was changed, false otherwise</returns>
        /// <exception cref="ArgumentNullException">Thrown if parameter was null</exception>
        public bool ChangeGoodsData(GoodsDTO goodsDTO)
        {
            if (goodsDTO == null)
                throw new ArgumentNullException(nameof(goodsDTO), "Argument equal null");

            var index = Database.Goods.FindIndex(x => x.Name == goodsDTO.Name);

            if (index == -1)
                return false;

            Database.Goods[index] = MyMapper.GoodsDTOToGoods(goodsDTO);
            return true;
        }

        public bool GoodsExist(string name)
        {
            return Database.Goods.Any(x => x.Name == name);
        }

        /// <summary>
        /// Receive goods by name or part of a name 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">Thrown if parameter was null</exception>
        public IEnumerable<GoodsDTO> FindGoodsByName(string name)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name), "Argument equal null");

            return MyMapper.GoodsListToGoodsDTOEnumerable(Database.Goods.FindAll(x => x.Name.ToLower().StartsWith(name.ToLower())));
        }
    }
}
