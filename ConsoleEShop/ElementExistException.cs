﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_BLL
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ElementExistException : Exception
    {
        public ElementExistException() { }

        public ElementExistException(string message)
            : base(message) { }

        public ElementExistException(string message, Exception inner)
            : base(message, inner) { }
    }
}