﻿using AutoMapper;
using ConsoleEShop_BLL.DTO;
using ConsoleEShop_Data.Entities;
using System;
using System.Linq;
using System.Collections.Generic;

namespace ConsoleEShop
{
     /// <summary>
     /// Mapper for all DTO classes
     /// </summary>
    public static class MyMapper
    {
        /// <summary>
        /// Mapping List<User> to IEnumerable<UserDTO>
        /// </summary>
        /// <param name="users"></param>
        /// <returns></returns>
        public static IEnumerable<UserDTO> UserListToUserDTOEnumerable(List<User> users)
        {
            var mapper = new MapperConfiguration(conf => conf.CreateMap<User, UserDTO>()).CreateMapper();

            return mapper.Map<List<User>, IEnumerable<UserDTO>>(users);
        }

        /// <summary>
        /// Mapping IEnumerable<UserDTO> to List<User>
        /// </summary>
        /// <param name="usersDTO"></param>
        /// <returns></returns>
        public static List<User> UserDTOEnumerableToUserList(List<UserDTO> usersDTO)
        {
            var mapper = new MapperConfiguration(conf => conf.CreateMap<UserDTO, User>()).CreateMapper();

            return mapper.Map<List<UserDTO>, List<User>>(usersDTO);
        }

        /// <summary>
        /// Mapping UserDTO to User
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        public static User UserDTOToUser(UserDTO userDTO)
        {
            var mapper = new MapperConfiguration(conf => conf.CreateMap<UserDTO, User>()).CreateMapper();

            return mapper.Map<UserDTO, User>(userDTO);
        }

        /// <summary>
        /// Mapping User to UserDTO
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static UserDTO UserToUserDTO(User user)
        {
            var mapper = new MapperConfiguration(conf => conf.CreateMap<User, UserDTO>()).CreateMapper();

            return mapper.Map<User, UserDTO>(user);
        }

        /// <summary>
        /// Mapping GoodsDTO to Goods
        /// </summary>
        /// <param name="goodsDTO"></param>
        /// <returns></returns>
        public static Goods GoodsDTOToGoods(GoodsDTO goodsDTO)
        {
            var mapper = new MapperConfiguration(conf => conf.CreateMap<GoodsDTO, Goods>()).CreateMapper();

            return mapper.Map<GoodsDTO, Goods>(goodsDTO);
        }

        /// <summary>
        /// Mapping Goods to GoodsDTO
        /// </summary>
        /// <param name="goods"></param>
        /// <returns></returns>
        public static GoodsDTO GoodsToGoodsDTO(Goods goods)
        {
            var mapper = new MapperConfiguration(conf => conf.CreateMap<Goods, GoodsDTO>()).CreateMapper();

            return mapper.Map<Goods, GoodsDTO>(goods);
        }

        /// <summary>
        /// Mapping List<Goods> to IEnumerable<GoodsDTO>
        /// </summary>
        /// <param name="goods"></param>
        /// <returns></returns>
        public static IEnumerable<GoodsDTO> GoodsListToGoodsDTOEnumerable(List<Goods> goods)
        {
            var mapper = new MapperConfiguration(conf => conf.CreateMap<Goods, GoodsDTO>()).CreateMapper();

            return mapper.Map<List<Goods>, IEnumerable<GoodsDTO>>(goods);
        }

        /// <summary>
        /// Mapping List<GoodsDTO> to List<Goods>
        /// </summary>
        /// <param name="goodsDTO"></param>
        /// <returns></returns>
        public static List<Goods> GoodsDTOListToGoodsList(List<GoodsDTO> goodsDTO)
        {
            var mapper = new MapperConfiguration(conf => conf.CreateMap<GoodsDTO, Goods>()).CreateMapper();

            return mapper.Map<List<GoodsDTO>, List<Goods>>(goodsDTO);
        }


        /// <summary>
        /// Mapping list of Order to IEnumerable<OrderDTO> without User class
        /// </summary>
        /// <param name="orders"></param>
        /// <returns></returns>
        public static IEnumerable<OrderDTO> OrderListToOrderDTOEnumerable(List<Order> orders)
        {
            var mapper = new MapperConfiguration(conf => conf.CreateMap<Order, OrderDTO>()
                                                             .ForMember(x => x.UserLogin, y => y.MapFrom(m => m.User.Login))
                                                             .ForMember(x => x.OrderStatusDTO, y => y.MapFrom(m => m.OrderStatus))
                                                             .ForMember(x => x.Goods, 
                                                                 y => y.MapFrom(m => MyMapper
                                                                    .GoodsListToGoodsDTOEnumerable(m.Goods.ToList()))))
                                                             .CreateMapper();

            return mapper.Map<List<Order>, IEnumerable<OrderDTO>>(orders);
        }
    }
}
