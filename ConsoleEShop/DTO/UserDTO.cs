﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_BLL.DTO
{
    /// <summary>
    /// The class corresponds to an entity User on the database layer
    /// </summary>
    public class UserDTO
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public UserTypeDTO Type { get; set; }


    }

    public enum UserTypeDTO
    {
        Guest = 1,
        RegisteredUser,
        Administrator
    }
}
