﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_BLL.DTO
{
    /// <summary>
    /// The class corresponds to an entity Goods on the database layer
    /// </summary>
    public class GoodsDTO
    {
        public string Name { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
    }
}
