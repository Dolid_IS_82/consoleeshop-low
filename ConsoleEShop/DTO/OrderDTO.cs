﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_BLL.DTO
{
    /// <summary>
    /// The class corresponds to an entity Order on the database layer
    /// </summary>
    public class OrderDTO
    {
        public int Id { get; set; }
        public string UserLogin { get; set; }

        public List<GoodsDTO> Goods { get; set; }

        public OrderStatusDTO OrderStatusDTO { get; set; }
    }

    public enum OrderStatusDTO
    {
        New = 1,
        CanceledByAdmin,
        PaymentReceived,
        Sent,
        Get,
        Completed,
        CanceledByUser
    }
}
