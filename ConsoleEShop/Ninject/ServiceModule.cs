﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEShop_Data;

namespace ConsoleEShop_BLL.Ninject
{
    /// <summary>
    /// Dependency injection for ListUnitOfWork
    /// </summary>
    public class ServiceModule : NinjectModule
    {


        public ServiceModule()
        {
        }

        public override void Load()
        {
            Bind<IListUnitOfWork>().To<ListUnitOfWork>();
        }
    }
}
