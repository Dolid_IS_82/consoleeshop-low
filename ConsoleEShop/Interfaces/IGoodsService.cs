﻿using ConsoleEShop_BLL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_BLL.Interfaces
{
    /// <summary>
    /// Service interface for working with goods 
    /// </summary>
    public interface IGoodsService
    {
        /// <summary>
        /// Get all available products 
        /// </summary>
        /// <returns></returns>
        IEnumerable<GoodsDTO> GetAllGoods();

        /// <summary>
        /// Receive item by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>One item with that name if exist</returns>
        GoodsDTO GetGoodsByName(string name);

        /// <summary>
        /// Add a new product 
        /// </summary>
        /// <param name="goodsDTO"></param>
        void AddGoods(GoodsDTO goodsDTO);

        /// <summary>
        /// Change the information about the available product 
        /// </summary>
        /// <param name="goodsDTO"></param>
        /// <returns>True if element was changed, false otherwise</returns>
        bool ChangeGoodsData(GoodsDTO goodsDTO);

        /// <summary>
        /// Check if there is a product with this name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>True if product exist, false otherwise</returns>
        bool GoodsExist(string name);

        /// <summary>
        /// Receive goods by name or part of a name 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        IEnumerable<GoodsDTO> FindGoodsByName(string name);
    }
}
