﻿using ConsoleEShop_BLL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_BLL.Interfaces
{
    public interface IUserService
    {
        /// <summary>
        /// Check if this login exists in the system 
        /// </summary>
        /// <param name="login"></param>
        /// <returns>True if that login exist, false otherwise</returns>
        bool LoginExist(string login);

        /// <summary>
        /// Check if there is a user with this login and password
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <returns>True if user exist, false otherwise</returns>
        bool UserExist(string login, string password);

        /// <summary>
        /// Add a new registered user 
        /// </summary>
        /// <param name="userDTO"></param>
        void AddUser(UserDTO userDTO);

        /// <summary>
        /// Get a list of all users in the system 
        /// </summary>
        /// <returns></returns>
        IEnumerable<UserDTO> GetAllUser();

        /// <summary>
        /// Get a user by this login 
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        UserDTO GetUserByLogin(string login);

        /// <summary>
        /// Edit user information 
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        bool ChangeUserData(UserDTO userDTO);
    }
}
