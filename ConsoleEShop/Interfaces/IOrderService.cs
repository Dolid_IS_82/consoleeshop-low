﻿using ConsoleEShop_BLL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_BLL.Interfaces
{
    public interface IOrderService
    {
        /// <summary>
        /// Get a list of orders made by this user 
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        IEnumerable<OrderDTO> GetOrdersByUserLogin(string login);

        /// <summary>
        /// Get a list of all orders
        /// </summary>
        /// <returns></returns>
        IEnumerable<OrderDTO> GetAllOrders();

        /// <summary>
        /// Change the status of order with thus id
        /// </summary>
        /// <param name="id">Id of the order</param>
        /// <param name="orderStatusDTO">New status</param>
        /// <returns>True if status was chnged, false otherwise</returns>
        bool ChangeOrderStatus(int id, OrderStatusDTO orderStatusDTO);

        /// <summary>
        /// Create a new order 
        /// </summary>
        /// <param name="goodsDTO">Set of goods in the ordered </param>
        /// <param name="login">The login of the user who placed this order </param>
        void CreateOrder(List<GoodsDTO> goodsDTO, string login);


    }
}
