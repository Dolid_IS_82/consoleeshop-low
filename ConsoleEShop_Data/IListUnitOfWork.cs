﻿using ConsoleEShop_Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Data
{
    public interface IListUnitOfWork
    {
        public List<Goods> Goods { get; }

        public List<Order> Orders { get; }

        public List<User> Users { get; }

        int Save();
    }
}
