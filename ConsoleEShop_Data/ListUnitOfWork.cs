﻿using ConsoleEShop_Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Data
{
    /// <summary>
    /// The class simulates work UnitOfWork
    /// </summary>
    public class ListUnitOfWork : IListUnitOfWork
    {

        public ListUnitOfWork()
        {
            SetStartData();
        }

        /// <summary>
        /// List of goods 
        /// </summary>
        public List<Goods> Goods { get; private set; }

        /// <summary>
        /// List of all orders 
        /// </summary>
        public List<Order> Orders { get; private set; }

        /// <summary>
        /// List of all users, both regular and admins 
        /// </summary>
        public List<User> Users { get; private set; }

        /// <summary>
        /// The method specifies the initial data in arrays 
        /// </summary>
        private void SetStartData()
        {
            Goods = new List<Goods>()
            {
                new Goods() { Name = "Water", Price = 15, Category = "Water", Description = "You can drink it"  },
                new Goods() { Name = "Bread", Price = 17, Category = "Baking ", Description = "You can eat it"  },
                new Goods() { Name = "Fanta", Price = 23, Category = "Water", Description = "Harmful to health"  },
                new Goods() { Name = "Cola", Price = 24, Category = "Water", Description = "Harmful to health "  },
                new Goods() { Name = "Cookies ", Price = 43, Category = "Baking", Description = "Very sweet "  },
                new Goods() { Name = "Chocolate ", Price = 27, Category = "Chocolate ", Description = "Harmful to teeth "  },
                new Goods() { Name = "Tomatoes  ", Price = 67, Category = "Vegetables", Description = "Very expensive in winter "  }
            };

            Users = new List<User>()
            {
                new User() { Login = "11111111", Password = "11111111", Name = "Kolya", Type = UserType.RegisteredUser},
                new User() { Login = "22222222", Password = "22222222", Name = "Vasya", Type = UserType.RegisteredUser},
                new User() { Login = "33333333", Password = "33333333", Name = "Vova", Type = UserType.Administrator},
                new User() { Login = "44444444", Password = "44444444", Name = "Vita", Type = UserType.RegisteredUser},
                new User() { Login = "55555555", Password = "55555555", Name = "Sasha", Type = UserType.RegisteredUser}
            };

            var tempGoods = new Goods() { Name = "Water", Price = 15, Category = "Water", Description = "You can drink it" };
            var tempOrder = new Order { User = Users[0], Goods = new List<Goods>() { tempGoods }, OrderStatus = OrderStatus.Completed };

            Orders = new List<Order>() { tempOrder };
        }

        public int Save()
        {
            return 1;
        }
    }
}
