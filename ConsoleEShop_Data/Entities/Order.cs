﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Data.Entities
{
    public class Order
    {
        /// <summary>
        /// The variable is responsible for incrementing the IDs to make them unique 
        /// </summary>
        private static int increment = 0;
        public int Id { get; set; }

        /// <summary>
        /// The user who placed the order 
        /// </summary>
        public virtual User User { get; set; }

        /// <summary>
        /// List of goods in the order 
        /// </summary>
        public virtual ICollection<Goods> Goods { get; set; }

        public OrderStatus OrderStatus { get; set; }

        public Order()
        {
            Id = ++increment;
            Goods = new List<Goods>();
        }
    }

    /// <summary>
    /// Enum of all order statuses
    /// </summary>
    public enum OrderStatus
    {
        New = 1,
        CanceledByAdmin,
        PaymentReceived,
        Sent,
        Get,
        Completed,
        CanceledByUser
    }


}
