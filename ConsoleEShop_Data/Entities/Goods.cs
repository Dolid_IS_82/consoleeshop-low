﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Data.Entities
{
    public class Goods
    {
        public string Name { get; set; }
        public string Category { get; set; }
        /// <summary>
        /// Short description of the order 
        /// </summary>
        public string Description { get; set; }
        public double Price { get; set; }
    }
}
