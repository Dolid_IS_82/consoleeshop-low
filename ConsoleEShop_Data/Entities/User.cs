﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Data.Entities
{
    public class User
    {
        /// <summary>
        /// The user's login is unique 
        /// </summary>
        public string Login { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public UserType Type { get; set; }

    }

    /// <summary>
    /// Application user types 
    /// </summary>
    public enum UserType
    {
        Guest = 1,
        RegisteredUser,
        Administrator
    }
}
